# MPWAR - David G. Shannon
[yo@epicdavid.com](mailto:yo@epicdavid.com)

## Ejercicio
Este repo es para guardar los ejercicios de Unit Testing de la asignatura de Desarrollo Eficiente.
Cada ejercicio se guardará en una branch diferente con el nombre del ejercicio.